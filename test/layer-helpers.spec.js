import { assert } from 'chai';

import {
  getLayersRoot,
  makeLayerArray,
  findVisibleLayers,
  findInvisibleLayers,
  // makeLayerStructure,
  makeLayersInvisible,
} from '../src/layer-helpers';

const EXAMPLE = require('./example-json.json');

describe('LayerHelpers', () => {
  describe('#getLayersRoot', () => {
    it('should provide access to layers and layer', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      assert(structureRoot.layers !== undefined, 'did not return structure with layers');
      assert(Array.isArray(structureRoot.layers), 'layers is not an array');
      assert(structureRoot.layers[0].layer !== undefined, 'did not return structure with layers followed by layer');
      assert(Array.isArray(structureRoot.layers[0].layer), 'layers.layer is not an array');
    });
  });

  describe('#makeLayerArray', () => {
    it('should create an array', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const layerArray = makeLayerArray(structureRoot);
      assert(Array.isArray(layerArray), 'did not return array');
    });

    it('should have 9 layers like example file', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const layerArray = makeLayerArray(structureRoot);
      assert(layerArray.length === 9, 'not all layers got in there');
    });

    it('each element should be a layer', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const layerArray = makeLayerArray(structureRoot);
      layerArray.forEach((layer) => {
        assert(layer.visible !== undefined, 'layer does not have visible property');
      });
    });
  });

  describe('#findVisibleLayers', () => {
    it('should create an array', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const visibleLayerArray = findVisibleLayers(structureRoot);
      assert(Array.isArray(visibleLayerArray), 'did not return array');
    });

    it('should have 7 layers like example file', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const visibleLayerArray = findVisibleLayers(structureRoot);
      assert(visibleLayerArray.length === 7, 'not all layers got in there');
    });

    it('each element should be a visible layer', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const visibleLayerArray = findVisibleLayers(structureRoot);
      visibleLayerArray.forEach((layer) => {
        assert(layer.visible !== undefined, 'layer does not have visible property');
        assert(layer.visible === '1', 'layer is actually invisible');
      });
    });
  });

  describe('#findInvisibleLayers', () => {
    it('should create an array', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const invisibleLayerArray = findInvisibleLayers(structureRoot);
      assert(Array.isArray(invisibleLayerArray), 'did not return array');
    });

    it('should have 2 layers like example file', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const invisibleLayerArray = findInvisibleLayers(structureRoot);
      assert(invisibleLayerArray.length === 2, 'not all layers got in there');
    });

    it('each element should be a invisible layer', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const invisibleLayerArray = findInvisibleLayers(structureRoot);
      invisibleLayerArray.forEach((layer) => {
        assert(layer.visible !== undefined, 'layer does not have visible property');
        assert(layer.visible === '0', 'layer is actually visible');
      });
    });

    it('should be complementary to the visible layers', () => {
      const structureRoot = getLayersRoot(EXAMPLE);
      const layerArray = makeLayerArray(structureRoot); 
      const visibleLayerArray = findVisibleLayers(structureRoot);
      const invisibleLayerArray = findInvisibleLayers(structureRoot);
      
      const combinedLength = visibleLayerArray.length + invisibleLayerArray.length;
      assert(layerArray.length === combinedLength, 'combined length of visible and invisible did not add up');
    });
  });
});
