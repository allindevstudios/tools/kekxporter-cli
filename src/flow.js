import {
  findAllFilesInFolderSync,
  makeTempFolder,
  changeFolder,
  changeExtension,
  unzipToFolder,
  copyFolder,
  sleep,
  zipFolder,
  createFolderSync,
} from 'file-helpers';

import { getMainDocument, makeMainDocumentXML } from 'kra-js';

import { dirname, extname } from 'path';

import { SpawnQueue } from 'spawn-limiter';
import { findVisibleLayers, getLayersRoot, makeLayerStructure, makeLayersInvisible, findInvisibleLayers, makeLayerVisible } from './layer-helpers';

// IDEA! Make a class with all these things as props -> done!
//  - though maybe a bit cheeky

let SETTINGS;
let spawnQueue;

const tempFolder = makeTempFolder('kexkporter-cli');

// TODO: totally missing the point atm...
// depth
export function setSettings(settings) {
  SETTINGS = settings;
}

export function initiateSpawnQueue(max) {
  if (max === 0) {
    spawnQueue = new SpawnQueue({ max: 99 });
  } else {
    spawnQueue = new SpawnQueue({ max });
  }
}

function getTempDestination(projectFile) {
  const currentDir = dirname(projectFile);
  const tempDestination = changeFolder(projectFile, currentDir, tempFolder);
  return changeExtension(tempDestination, `${extname(projectFile)}`, '');
}

export function importFile(projectFile) {
  const tempDestination = getTempDestination(projectFile);
  const baseFolder = `${tempDestination}/base`;
  createFolderSync(`${tempDestination}/temp`);
  const destination = SETTINGS.destination ? `${SETTINGS.destination}/result` : `${tempDestination}/result`;
  console.log('DESTINATION:', destination);
  createFolderSync(destination);
  return new Promise(async (resolve, reject) => {
    const { depth } = SETTINGS;
    // extract
    await unzipToFolder(projectFile, baseFolder);
    // const documentInfo = await getDocumentInfo(baseFolder);
    const mainDocument = await getMainDocument(baseFolder);
    const layersRoot = getLayersRoot(mainDocument);
    
    const visibleLayers = findVisibleLayers(layersRoot, depth);
    makeLayersInvisible(layersRoot, depth);

    Promise.all(visibleLayers.map(async (visibleLayer) => {
      const documentForExport = JSON.parse(JSON.stringify(mainDocument));
      const layersForExport = getLayersRoot(documentForExport);
      

      makeLayerVisible(layersForExport, visibleLayer.uuid);
      
      const copiedFolder = `${tempDestination}/temp/${visibleLayer.name}`;
      await copyFolder(`${tempDestination}/base`, copiedFolder);
      await sleep(100);

      await makeMainDocumentXML(documentForExport, copiedFolder);
      await sleep(50);
      const kraFile = `${tempDestination}/temp/${visibleLayer.name}.kra`;
      await zipFolder(copiedFolder, kraFile);
      await sleep(100);
      const resultFile = `${destination}/${visibleLayer.name}.png`;
      // console.log(visibleLayer);
      console.log('Starting on layer', visibleLayer.name);
      const kritaArgs = [kraFile, '--export', '--export-filename', resultFile];
      spawnQueue.addCommand('krita', kritaArgs);
    }))
      .then(() => resolve())
      .catch(err => reject(err));
    
    resolve();
  });
}

export function importProjectFolder(projectFolder) {
  return new Promise((resolve, reject) => {
    const files = findAllFilesInFolderSync(projectFolder, '.kra');

    const promises = Promise.all(files.map(file => importFile(file)));
    promises.then(() => {
      resolve();
    });
  });
}
