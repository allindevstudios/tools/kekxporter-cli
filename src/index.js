import parseArgs from 'minimist';

import {
  isFolderSync,
} from 'file-helpers';

import {
  existsSync,
} from 'fs';

import {
  extname,
} from 'path';

import {
  initiateSpawnQueue,
  importFile,
  importProjectFolder,
  setSettings,
} from './flow';

/**
 * TODO:
 * interesting point is raised where for a bottom layer to be visible that all
 * layers in that path need to be turned visible as well, not just the top one
 */

const USAGE = `
Kekxporter usage
================
  yarn start [source] [destination] --depth=number --concurrent=number --autocrop

  Flags
  -----
  --help, print usage

  --depth, specifies the layer depth in which the program will export the layers.
      Higher means more layers and thus a longer time. Set 0 for all layers.
  --autocrop, default value false. Autocrops the images after exporting.
  --concurrent, number of concurrent Krita processes that may be running,
      if not a lot of RAM keep this low. Default 5. Set 0 for no limit.
`;

function printUsageAndExit() {
  console.log(USAGE);
  process.exit();
}

function main() {
  const args = parseArgs(process.argv.slice(2));

  const settings = {};

  if (args.help) {
    printUsageAndExit();
  }

  if (args.depth === undefined || Number.isNaN(args.depth)) {
    console.error('ERROR: No depth specified Please specify a depth for the program using --depth=X flag');
    printUsageAndExit();
  }

  if (args._.length === 0) {
    console.error('ERROR: No folder specified. Please specify a source folder for the program. To specify the local folder, use "."');
    printUsageAndExit();
  }

  if (args._.length === 1) {
    console.error('WARNING: No destination specified. Please specify a destination folder for the program. To specify the local folder, use "."');
    // printUsageAndExit();
  } else {
    settings.destination = args._[1];
  }


  if (args.concurrent === undefined) {
    initiateSpawnQueue(5);
  } else {
    initiateSpawnQueue(args.concurrent);
  }

  const sourcePath = args._[0];

  settings.depth = args.depth === 0 ? undefined : args.depth;
  setSettings(settings);

  if (existsSync(sourcePath)) {
    if (isFolderSync(sourcePath)) {
      importProjectFolder(sourcePath);
    } else {
      if (extname(sourcePath) !== '.kra') {
        console.warn('WARNING: File is not a recognized Krita file (.kra extension).', extname(sourcePath));
      }
      importFile(sourcePath);
    }
  } else {
    console.error(`ERROR: "${sourcePath}" does not exist`);
    // printUsageAndExit();
    process.exit();
  }
}


main();
