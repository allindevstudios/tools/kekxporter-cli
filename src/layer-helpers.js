/**
 *
 * @param {*} structure 
 */
export function getLayersRoot(structure) {
  return structure.DOC.IMAGE[0];
}

// wishful thinking for now.
export function layerIterator(structure, layerFunction, depth, defaultValue) {
  if (depth !== undefined && depth <= 0) {
    return defaultValue;
  }

  const layers = structure.layers ? structure.layers[0].layer : [];

  layers.forEach(layerFunction);

  // change
  return defaultValue;
}

/**
 *
 * @param {*} structure 
 * @param {*} depth 
 */
export function makeLayersInvisible(structure, depth) {
  if (depth !== undefined && depth <= 0) {
    return;
  }

  const layers = structure.layers ? structure.layers[0].layer : [];

  layers.forEach((layer) => {
    const layerProperties = layer.$;
    if (layerProperties.visible === '1') {
      layerProperties.visible = '0';
    }
    makeLayersInvisible(layer, depth === undefined ? depth : depth - 1);
  });
}

/**
 *
 * @param {*} structure
 */
export function makeLayerArray(structure, depth) {
  if (depth !== undefined && depth <= 0) {
    return [];
  }
  let array = [];
  const layers = structure.layers ? structure.layers[0].layer : [];

  layers.forEach((layer) => {
    array.push(layer.$);
    array = array.concat(makeLayerArray(layer, depth === undefined ? depth : depth - 1));
  });

  return array;
}

/**
 *
 * @param {*} structure 
 * @param {*} layerId 
 */
export function makeLayerVisible(structure, layerId) {
  if (structure.layers === undefined) {
    return false;
  }

  for (let i = 0; i < structure.layers[0].layer.length; i += 1) {
    const layer = structure.layers[0].layer[i];
    const layerProperties = layer.$;
    if (layerProperties.uuid === layerId) {
      layerProperties.visible = '1';
      return true;
    }
    const isASubLayer = makeLayerVisible(layer, layerId);
    if (isASubLayer) {
      layerProperties.visible = '1';
      return true;
    }
  }
  return false;
}

/**
 * Makes a structure for easier visualisation, not that relevant right now
 * @param {*} structure
 */
export function makeLayerStructure(structure, depth) {
  const layerStructure = {};
  const layers = structure.layers ? structure.layers[0].layer : [];
  layers.forEach((layer) => {
    const layerProperties = layer.$;
    layerStructure[layerProperties.name] = {
      properties: layerProperties,
      layers: makeLayerStructure(layer),
    };
  });
  return layerStructure;
}

export function findVisibleLayers(structure, depth) {
  return makeLayerArray(structure, depth).filter(layer => layer.visible === '1');
}


export function findInvisibleLayers(structure, depth) {
  return makeLayerArray(structure, depth).filter(layer => layer.visible === '0');
}
